import React, { Component } from 'react'
import styled from 'styled-components'

import Plane from '../../components/Plane2'
import UserAvatar from '../../components/UserAvatar';

const FieldWrap = styled.div`
    width: 100%;
    height: 200vh;
    overflow: hidden;
`

const Field = styled.div`
  transform: ${props => `scale(${props.scale})`};
}
`

class ViewPort extends Component {
  state = {
    scale: 1
  }

  componentDidMount(){
    window.addEventListener('scroll', this.scrollScale)
    window.addEventListener('onmousedown', this.dragging)
  }

  componentWillUnmount(){
    window.removeEventListene('scroll', this.scrollScale)
  }

  scrollScale = () => {
    var scroll = document.documentElement.scrollTop || document.body.scrollTop;
    this.setState({
      scale:  1+scroll*0.001
    })
  }

  render(){
  const { scale } = this.state
  const {users, handleDrop} = this.props
  return (
    <FieldWrap >
      <Plane scale={scale} handleDrop={handleDrop} />
      <Field style={{ transform: `scale(${scale})` }}>
            {users.map(item => {
            return <UserAvatar handleDrop={handleDrop} key={item.id} item={item}/>
            })}
      </Field>
     </FieldWrap>
  )
}
}

export default ViewPort
