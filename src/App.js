import React from 'react';
import styled from 'styled-components'

import Header from './containers/Header'
import ViewPort from './containers/ViewPort'

const AppWrap = styled.div`
  width:100%;
  position: relative;
  overflow: hidden;
  background: #121212;
`

class App extends React.Component {
  state = {
    users: [
      {
        name: "Misha  Mar",
        id: '1',
        coords: {
          x: "200",
          y: "400"
        }
      },
      {
        name: "Sasha Pylypenko",
        id: '2',
        coords: {
          x: "50",
          y: "150"
        }
      },
        {
          name: "Sasha Pylypenko",
          id: '3',
          coords: {
            x: "0",
            y: "30"
          }
        },
          {
            name: "Sasha Pylypenko",
            id: '4',
            coords: {
              x: "225",
              y: "125"
            }
      }
    ]
  }

  handleDrop = ({ id, coords }) => {
    const { users } = this.state;
    const arr = [...users]
    const idx = arr.findIndex(i => i.id === id);
    arr[idx].coords = coords
    this.setState({
      users: arr
    })
  }
  render(){
    const { users } = this.state;
    return (
        <AppWrap>
          <Header />
          <ViewPort users={users} handleDrop={this.handleDrop} />
        </AppWrap>
    );
  }
}

export default App

