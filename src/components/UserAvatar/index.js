import React, { useEffect, useState, useRef  } from 'react';
import styled from 'styled-components'
import { getCoords } from '../../utils/help-func'

const Wrap = styled.div`
 position: absolute;
  width: 128px;
  height: 128px;
  border: 2px solid #fff;
  border-radius: 50%;
  background: #c4c4c4;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
  color: #fff;
  font-weight: bold;
     cursor: move; /* fallback if grab cursor is unsupported */
    cursor: grab;
    cursor: -moz-grab;
    cursor: -webkit-grab;
    video {
    transform: scale(-1.5,1.5)!important;
    transform-origin: 50% 50%!important;
}
  `

const UserAvatar = ({item, handleDrop}) => { 
  const { name, coords, id } = item
  const [media, setMedia] = useState(null)
  let UserTag = useRef(null)
  let videoTag = useRef(null)

  useEffect(() => {
   getUserMedia()
  },[])

  useEffect(() => {
   const current = UserTag.current;
   current.ondragstart = function() {
     return false;
   };

   current.onmousedown = (e) => {

     let objCoords = getCoords(current);
     let left = e.pageX - objCoords.left;
     let top = e.pageY - objCoords.top;


     const moveAt = (e) => {
       let x = e.pageX - left;
       let y  = e.pageY - top;
       handleDrop({id, coords: {x, y}})
     }

     moveAt(e);
   
     document.onmousemove = (e) =>  {
       moveAt(e);
     };
   
     current.onmouseup = ()  => {
       document.onmousemove = null;
       current.onmouseup = null;
     };
   
   }
   return () => {
   };
 },[coords, handleDrop, id])

  useEffect(() => {
    if(videoTag.current){
     videoTag.current.srcObject = media
    }
   },[media])

  const getUserMedia = () => {
    navigator.getUserMedia = navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia;

    if (navigator.getUserMedia) {
     navigator.getUserMedia({ audio: true, video: {width: 100, height: 100} },
        stream => {
           setMedia(stream)
        },
        err => {
           console.error("The following error occurred: " + err.name);
        }
     );
  } else {
     console.error("getUserMedia not supported");
  }
  }

  const video = <video ref={videoTag}  autoPlay={true} playsInline={true}>Sorry, Web RTC is not available in your browser</video>
  return (
        <Wrap ref={UserTag} style={coords && {left: coords.x + "px",top: coords.y + "px"}}>
         {media ? video : name}
        </Wrap>
    );
  }

export default UserAvatar