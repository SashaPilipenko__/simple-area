import React, { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'

import { getCoords } from '../../utils/help-func'

const PlaneWrap = styled.div`
  width: 5000px;
  height: 5000px;
  position: absolute;
  background: url('https://image.winudf.com/v2/image/Y29tLmJsYWNrYmFja2dyb3VuZHdhbGxwYXBlcnNpbWFnZXNfc2NyZWVuXzJfMTUwOTI1MjEyOV8wOTU/screen-2.jpg?fakeurl=1&type=.jpg');
  cursor: move;
  `

const Plane = ({ children, scale }) => {
  const [coords, serCoords] = useState(null)
  let PlaneRef = useRef(null)

  useEffect(() => {
    const current = PlaneRef.current;
    current.ondragstart = function() {
      return false;
    };
    // current.onmousedown = (e) => {
    //   dragging(e)
    //   document.onmousemove = (e) => {
    //     dragging(e)
    //   }
    //   current.onmouseup = (e) => {
    //     document.onmousemove = null;
    //     current.onmouseup = null;
    //   }
    // }
    current.onmousedown = (e) => {
      
      let objCoords = getCoords(current);
      let left = e.pageX - objCoords.left;
      let top = e.pageY - objCoords.top;
      let right = e.pageX + objCoords.right;
      let bottom = e.pageY + objCoords.bottom;


      const moveAt = (e) => {
        let x = e.pageX - left;
        let y  = e.pageY - top;
        let rightC = e.pageX - right;
        let bottomC  = e.pageY - bottom;

        console.log(rightC, bottomC)
        if(x > 0 || x <  -5000){
          x = 0;
        }
        if(y > 0 || y < -5000){
          y = 0;
        }
        serCoords({ x, y })
      }

      moveAt(e);
    
      document.onmousemove = (e) =>  {
        moveAt(e);
      };
    
      current.onmouseup = ()  => {
        document.onmousemove = null;
        current.onmouseup = null;
      };
    
    }
    return () => {
    };
  },[coords])

    return (
        <PlaneWrap style={coords && {left: coords.x + "px",top: coords.y + "px", transform: `scale(${scale})`}} ref={PlaneRef}>
          {children}
        </PlaneWrap>
    )
}

export default Plane