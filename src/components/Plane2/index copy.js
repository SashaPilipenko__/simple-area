import React, { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'

import { getCoords } from '../../utils/help-func'

const PlaneWrap = styled.canvas`
  width: 5000px;
  height: 5000px;
  position: absolute;
  background: url('https://image.winudf.com/v2/image/Y29tLmJsYWNrYmFja2dyb3VuZHdhbGxwYXBlcnNpbWFnZXNfc2NyZWVuXzJfMTUwOTI1MjEyOV8wOTU/screen-2.jpg?fakeurl=1&type=.jpg');
  cursor: move;
  `

const Plane = ({ children, scale }) => {
  const [coords, serCoords] = useState(null)
  let PlaneRef = useRef(null)

  useEffect(() => {
    const current = PlaneRef.current;
    if (current.getContext) {
      console.log('yees')
    }
    return () => {
    };
  },[coords])

    return (
        <PlaneWrap style={coords && {left: coords.x + "px",top: coords.y + "px", transform: `scale(${scale})`}} ref={PlaneRef}>
          {children}
        </PlaneWrap>
    )
}

export default Plane