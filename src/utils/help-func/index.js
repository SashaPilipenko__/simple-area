export const getCoords = (elem) => { // кроме IE8-
  var box = elem.getBoundingClientRect();
  console.log(box)
  return {
    top: box.top + window.pageYOffset,
    left: box.left + window.pageXOffset,
    bottom: box.bottom - window.pageYOffset,
    right: box.right - window.pageXOffset
  };

}